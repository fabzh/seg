# coding: utf-8

from datetime import datetime
import elasticsearch
import hashlib
import signal
import time

class Segparser:

    def __init__(self):
        # First, let's deal with graceful stop
        self.gracefully_stopped = False
        signal.signal(signal.SIGTERM, self.exit_gracefully)
        self.retry_connection_delay = 3
        self.main_loop_delay = 10
        self.es = elasticsearch.Elasticsearch(['elasticsearch:9200'])

        # Loop : try to create indices until elasticsearch is ready
        while True:
            try:
                print(f"Trying to connect...")
                self.es.indices.create(index='seg-metrics-index', ignore=400)
                self.es.indices.create(index='seg-filess-index', ignore=400)
                break
            except elasticsearch.exceptions.ConnectionError as e:
                print(e)
                print(f"Waiting {self.retry_connection_delay} before trying again...")
                time.sleep(self.retry_connection_delay)
        print("Connected")

    def exit_gracefully(self, signum, frame):
        print("Received SIGTERM: segparser will stop at the end of the main loop...")
        self.gracefully_stopped = True

    def main_loop(self):
        while not self.gracefully_stopped:
            time.sleep(self.main_loop_delay)

        print("Stopped")

if __name__ == "__main__":
    sp = Segparser()
    sp.main_loop()